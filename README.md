# IOS-Project4

After school / internships are over, it is actually less common (for most application developers) to work on a project from the ground up.

It is far more common to come onto a project that already exists.. sometimes they're really big projects that have been around for a really long time... and you 
desperately want to rewrite everything about them... but you will never have time and your company will never pay for it. (It's the stuff of nightmares really.)

Don't worry, this one isn't that bad! 

In this project, you will add some user stories to an already working app. And you will have to fix one small oversight.

It's not a huge project, but it does mean: 
* you won't know how everything works
* you may not need to know how everything works to finish the project (in fact, you definitely won't!)
* you will have to figure out other people's code
* you may have to research some parts that you don't understand, or ask questions in class or Canvas
* you will have to do some good 'ole fashioned trial-and-error learning

Remember to use the debugger to learn about the project, and to use Cmd-Click on types to see where they are defined. 

The app itself is a Workout Tracking app, which right now has the ability to add workouts and see them in a list. 
It also persists the workout to device, so that if a user terminates the app, or restarts their phone, their workouts are saved.

## PROJECT 4 REQUIREMENTS


User Stories To Complete

The exact details of how to implement these stories are up to you. 

### Fix:
As a user, I should not be able to make an exercise that is less than 2 minutes long.

### Story 1:
As a user, I want to be able to specify how many calories are burned per minute when I create a Workout, instead of specifying whether the exercise is high intensity or not. (I should not be able to enter less than 1 calories burned per minute.)

### Story 2:
As a user, I want to be able to see the total calories burned for each Workout on the Workout List screen.

### Story 3: 
As a user, I want to be able to sort my workouts on the Workout List screen by date (ascending), by date (descending), by calories burned (descending), or by duration (descending).

### Story 4: 
As a user, I want to be able to edit my workouts in the event I made a mistake creating one in the first place. Pay extra attention here to ids, as they are a workout's primary identifier at a persistence level. You will have to fix a bug in the current workflow surrounding duplicate ids to get this feature to work. Also take note that a storyboard segue has already been created for you (with an identifier) in WorkoutList.storyboard. Some aspects of editing are already hooked up but you will have to fill in certain gaps. MAKE SURE EDITING PASES VALIDATION BEFORE SAVING.

### Story 5:
As a user, I want to be able to remove all of my workouts from my workout list.

### Additional Requirements

- Auto Layout: The app is currently built using Auto Layout, so you need to continue to use Auto Layout on any changes you make to the UI or any additional Views and View Controllers you add.

- Persistence:  The app currently persists the user's workout list to disk every time a Workout is saved to the WorkoutListModel. You just need to make sure that any changes you make to the Workout structure are still persisted correctly. While you're in the Simulator, press Cmd-Shift-H twice quickly to open up the currently active apps screen. Drag the Project 4 app up to terminate it. Then you can re-run the app to see if the data you saved persisted correctly.


### SUBMISSION

- As with all projects, your project should run on Xcode 8.2 and be free of compile-time errors and crashes (run-time errors). Those will have significant deductions.

- You should also not have any compile-time warnings (in yellow).

- When you're ready to submit, rename the folder containing the entire Xcode project and source folder to your UMSL user id (e.g. 'gmhz7b'). Then right click on that folder and select the option that should say "Compress gmhz7b" (or whatever your user id is). Finally upload that zip file to Canvas.

- Make sure you give yourself enough time to make the submission.

- It's always better to submit something than nothing!
